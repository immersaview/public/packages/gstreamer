mkdir c:\temp

Write-Host "Installing Visual Studio Components"; `
$buildtools_dl_url = 'https://aka.ms/vs/16/release/vs_buildtools.exe';
Invoke-WebRequest -Uri $buildtools_dl_url -OutFile C:\temp\vs_buildtools.exe;

Start-Process -FilePath C:/TEMP/vs_buildtools.exe -ArgumentList "--quiet", "--norestart", "--wait", "--nocache",
    "--add", "Microsoft.VisualStudio.Workload.VCTools",
    "--add", "Microsoft.VisualStudio.Component.VC.ATL",
    "--add", "Microsoft.VisualStudio.Component.VC.CMake.Project",
    "--add", "Microsoft.VisualStudio.Component.VC.Tools.x86.x64",
    "--add", "Microsoft.VisualStudio.Component.Windows10SDK.18362",
    "--add", "Microsoft.VisualStudio.Workload.NetCoreBuildTools",
    "--add", "Microsoft.Component.VC.Runtime.UCRTSDK" -Wait -NoNewWindow;

iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex;
choco install git -y --params="'/GitAndUnixToolsOnPath /NoGitLfs /NoShellIntegration'"  --version 2.30.2;
$env:PATH = [System.Environment]::GetEnvironmentVariable('PATH', 'Machine') + ';' + 'C:\Program Files\Git\bin';


Write-Host "Installing pkg-config-lite"; `
choco install pkgconfiglite -y --version 0.28;
$env:PATH = [System.Environment]::GetEnvironmentVariable('PATH', 'Machine') + ';' + 'C:\ProgramData\chocolatey\lib\pkgconfiglite\tools\pkg-config-lite-0.28-1\bin';
[Environment]::SetEnvironmentVariable('PATH', $env:PATH, [EnvironmentVariableTarget]::Machine);


Write-Host "Installing Python";
$python_dl_url = 'https://www.python.org/ftp/python/3.9.2/python-3.9.2-amd64.exe';
Invoke-WebRequest -Uri $python_dl_url -OutFile C:\temp\python3-installer.exe;
Start-Process C:\temp\python3-installer.exe -ArgumentList '/quiet InstallAllUsers=1 AssociateFiles=1 PrependPath=1 TargetDir=C:\Python39' -Wait;
$env:PATH = [System.Environment]::GetEnvironmentVariable('PATH', 'Machine') + ';' + 'C:\Python39';

Write-Host "Installing OpenSSL";
choco install openssl --version 1.1.1.1100 -y

Write-Host "Configuring git";
git config --global user.email gst-build@immersaview.com;
git config --global user.name ImmersaView;
git config --global core.autocrlf false;

Write-Host "Installing ninja";
pip3 install ninja
$env:PATH = [System.Environment]::GetEnvironmentVariable('PATH', 'Machine') + ';' + 'C:\Python39\Lib\site-packages\ninja\data\bin';

Write-Host "Cloning Meson"
mkdir c:\dev
cd c:\dev
git clone https://github.com/mesonbuild/meson.git
cd meson
git checkout -t origin/0.57
New-Item c:\dev\meson.cmd
Set-Content c:\dev\meson.cmd '@echo off

c:\dev\meson\meson.py %*'

$env:PATH = [System.Environment]::GetEnvironmentVariable('PATH', 'Machine') + ';' + 'C:\dev';
[Environment]::SetEnvironmentVariable('PATH', $env:PATH, [EnvironmentVariableTarget]::Machine);


Write-Host "Cloning gst-build"
mkdir c:\dev\gstreamer
cd c:\dev\gstreamer
git clone https://gitlab.com/immersaview/public/remotes/gst-build.git
cd gst-build
git checkout -t origin/1.18.1-imv

$env:SOURCE_DIR = 'c:\dev\gstreamer\';
[Environment]::SetEnvironmentVariable('SOURCE_DIR', $env:SOURCE_DIR, [EnvironmentVariableTarget]::Machine);

$env:GSTREAMER_1_0_ROOT_X86_64 = $env:SOURCE_DIR + 'x86_64\'
[Environment]::SetEnvironmentVariable('GSTREAMER_1_0_ROOT_X86_64', $env:GSTREAMER_1_0_ROOT_X86_64, [EnvironmentVariableTarget]::Machine);

$env:GST_SRC_BUILD_PATH = $env:SOURCE_DIR + 'gst-build\build\subprojects\'
[Environment]::SetEnvironmentVariable('GST_SRC_BUILD_PATH', $env:GST_SRC_BUILD_PATH, [EnvironmentVariableTarget]::Machine);

$env:GST_PLUGIN_PATH = $env:GST_SRC_BUILD_PATH + 'gst-plugins-good;' + $env:GST_SRC_BUILD_PATH + 'gst-plugins-bad;' + $env:GST_SRC_BUILD_PATH + 'gst-plugins-ugly;' + $env:GST_SRC_BUILD_PATH + 'gst-plugins-base'
[Environment]::SetEnvironmentVariable('GST_PLUGIN_PATH', $env:GST_PLUGIN_PATH, [EnvironmentVariableTarget]::Machine);

$env:PKG_CONFIG_PATH = $env:GSTREAMER_1_0_ROOT_X86_64 + 'lib\pkgconfig'
[Environment]::SetEnvironmentVariable('PKG_CONFIG_PATH',  $env:PKG_CONFIG_PATH, [EnvironmentVariableTarget]::Machine);

$env:PATH = [System.Environment]::GetEnvironmentVariable('PATH', 'Machine') + ';' + $env:GSTREAMER_1_0_ROOT_X86_64 +'bin'
[Environment]::SetEnvironmentVariable('PATH', $env:PATH, [EnvironmentVariableTarget]::Machine);


Remove-Item -Force -Recurse "c:\temp";
