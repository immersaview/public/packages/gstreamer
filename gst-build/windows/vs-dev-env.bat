@echo off
cd C:\dev\gstreamer\gst-build
echo *
echo *****************************************************************************
echo ** Setup             "meson setup -Dbad=enabled -Dgst-plugins-bad:hls=disabled -Dglib-networking:openssl=enabled -Dglib-networking:installed_tests=disabled -Dlibnice:crypto-library=openssl --prefix=%%GSTREAMER_1_0_ROOT_X86_64%% build_x64"
echo ** Build             "meson compile -C build_x64"
echo ** Build and install "meson install -C build_x64"
echo *****************************************************************************
echo *
%comspec% /k "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvars64.bat"
