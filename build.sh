# On Windows, this script is intended to be run via MinGW:
#   C:\Windows\Syswow64\cmd.exe /C msys-shell.bat build.sh
# https://code.immersaview.com/imv/docker/docker-images/-/blob/master/ci-gst-cerbero-build-deps/msys-shell.bat

set -e

# May be one of: windows, centos, debian
G_OS="${1}"
# Either debug or release
G_CONFIG="${2}"
# Either fetch, build or configure
G_ACTION="${3}"

if [[ "${#}" != "3" ]]; then
    echo "Usage:"
    echo "  build.sh <OS> <CONFIG> <ACTION>"
    exit 1
fi

G_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE}")" && pwd)"
G_CERBERO_CMD="./cerbero-uninstalled --timestamps"
G_CERBERO_VARIANTS=""
G_SOURCE_CACHE="${G_SCRIPT_DIR}/.cache/cerbero-sources"

if [[ "${G_OS}" == "windows" ]]; then
    G_CERBERO_CMD="python ${G_CERBERO_CMD}"
    G_CERBERO_VARIANTS="${G_CERBERO_VARIANTS} --variants visualstudio"

    # Linux paths seem to break cerbero on windows
    G_SOURCE_CACHE="$(cygpath.exe --mixed ${G_SOURCE_CACHE})"
fi

if [[ "${G_CONFIG}" == "release" ]]; then
    G_CERBERO_VARIANTS="${G_CERBERO_VARIANTS} --variants gst_level_max_fix_me"
fi

# Workaround for:
#   'cerbero.errors.FatalError: Fatal Error: Don't know how to combine the environment variable 'PERL5LIB' with values'...
unset PERL5LIB

pushd ${G_SCRIPT_DIR}/cerbero
    case "${G_ACTION}" in
        "config"*)
            {
                mkdir --parents ~/.cerbero
                rm --force ~/.cerbero/cerbero.cbc

                echo "local_sources=\"${G_SOURCE_CACHE}\"" >> ~/.cerbero/cerbero.cbc

                if [[ "${G_OS}" != "windows" ]]; then
                    # fix for 'warning: no packager defined, using default packager "default <default@change.me>"'
                    #   https://marc.info/?l=gstreamer-bugs&m=152564821216665&w=2
                    perl -pi -e 's/[^[:ascii:]]//g' packages/gstreamer-1.0/license.txt
                fi

                ${G_CERBERO_CMD} ${G_CERBERO_VARIANTS} show-config
            }
            ;;
        "fetch")
            {
                ${G_CERBERO_CMD} fetch-bootstrap
                ${G_CERBERO_CMD} fetch-package gstreamer-1.0

                # Manually fetch packages that aren't fetched by the above
                # Probably due to using an unsupported version of Ubuntu and trying to keep the cache the same on all platforms
                # https://code.immersaview.com/remotes/cerbero/-/blob/1.16.1-imv/cerbero/bootstrap/linux.py#L87
                ${G_CERBERO_CMD} fetch glib-networking
            }
            ;;
        "build")
            {
                # bootstrap doesn't update so we need to
                if [[ "${G_OS}" == "debian" ]]; then
                    sudo apt-get update
                fi

                ${G_CERBERO_CMD} bootstrap --assume-yes
                ${G_CERBERO_CMD} ${G_CERBERO_VARIANTS} package --tarball gstreamer-1.0
            }
            ;;
        *)
            echo "Supply ACTION argument (fetch|build|configure)"
            ;;
    esac
popd
