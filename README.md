**Nuget packaging for GStreamer**

## Example CMake Usage:
```cmake
target_link_libraries(target gstreamer)
copy_dynamic_lib(target gstreamer gmodule glib gobject ffi intl)
```

## Building GStreamer with gst-build

### Windows
```
gstreamer
└───gst-build
   └───windows
       │   provision-gst-build.ps1
       |   vs-dev-env.bat
```
#### Setup
1. Create a new VM in Hyper-V with Quick Create selecting the **Windows 10 dev environment** option.
1. In the VM settings make sure it has at least 8GB of ram.
1. Start the VM.
1. Open Visual Studio and exit immediately.  (The provisioning script puts Visual Studio in a broken
   state if its run first.)
1. Ensure you have interent access.
1. Copy `provision-gst-build.ps1` and `vs-dev-env.bat` to the VM.
1. Open an instance of powershell with admin privileges.
1. At the prompt run `Set-ExecutionPolicy Bypass -Scope Process -Force`
1. Run the provisioning script `provision-gst-build.ps1`.
1. Close the terminal

This installs the following
* Visual Studio Build Tools
* pkg-config-lite
* Python
* git
* openssl
* meson
* A clone of gst-build

#### Development
1. Run the dev environment script `vs-dev-env.bat`.  This starts a Visual Studio shell in the gst-build
   directory.
1. At the prompt run
 `meson setup -Dbad=enabled -Dgst-plugins-bad:hls=disabled -Dglib-networking:openssl=enabled -Dglib-networking:installed_tests=disabled -Dlibnice:crypto-library=openssl --prefix=%GSTREAMER_1_0_ROOT_X86_64% build_x64`
   This downloads and builds all the gstreamer dependencies. You will notice I've disabled `hls` and `glib-networking` tests. This was required to get `gst-plugins-bad` to build.
1. Run `meson compile -C build_x64` to build
1. Optionally run `meson install -C build_x64` to install.  Note after installing gst-launch-1.0 will be available at the commmand line.

#### Notes
* All the build options and their descriptions can be found in the build (`build_x64`) directory under `meson-info/intro-buildoptions.json`.
* Meson defaults to building in Debug (optimised Debug on Linux), to build in Release add `--buildtype=release` to the meson config
  step. If you have already configured meson for debug you will need to add `--reconfigure` as well. For
  more options see https://mesonbuild.com/Builtin-options.html.
* Usefull Meson Options
   * `--prefex=<path>` Installation directory. Where gstreamer will be installed.
   * `--buildtype=<config>` Build type. Available options are `plain, debug, debugoptimized, release, minsize, custom`
     * Note that [FFmpeg won't build without some level of optimisation](https://trac.ffmpeg.org/ticket/6429), aka with `--buildtype=debug`. FFmpeg can be disabled by adding `-Dlibav=disabled` to the meson setup.
   * `--libdir=<path>` Library directory.  Path within the Installation directory where libraries are installed.
* These scripts were built from information from the following following pages.
   * [Building GStreamer on Windows](https://www.collabora.com/news-and-blog/blog/2019/11/26/gstreamer-windows/)
   * [Getting started with GStreamer's gst-build](https://www.collabora.com/news-and-blog/blog/2020/03/19/getting-started-with-gstreamer-gst-build/)
   * https://github.com/GStreamer/gst-build
* The provisioning script installs **Visual Studio Build Tools** which is sepearate to the toolchain used
  in **Visual Studio Community Edition** that comes preinstalled with the VM.


## Updating GStreamer to a new version.
  After winding forward both repositories listed below to their new versions, the following changes will need to be re-applied.
  1. https://gitlab.com/immersaview/public/remotes/cerbero
     * `recipes\gst-plugins-bad-1.0.recipe` will need to be modified to point to the [local mirror](https://gitlab.com/immersaview/public/remotes/gst-plugins-bad/-/archive/1.18.1/gst-plugins-bad-1.18.1.tar.gz) and the tarball checksum updated.  The tarball_checksum is a SHA256. If you're on
      windows you can use the `certUtil` utility. e.g. *certUtil -hashfile gst-plugins-bad-1.18.1.tar.gz SHA256*.
  1. https://gitlab.com/immersaview/public/remotes/gst-build
    * `subprojects/gst-plugins-bad.wrap` needs both `url` and `push-url` changed to point to the [local mirror](https://gitlab.com/immersaview/public/remotes/gst-plugins-bad).


