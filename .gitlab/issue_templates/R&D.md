## Hypothesis

Idea or explanation of what you hope to achieve through experimentation and investigation.

## Requirements (Optional)
- [ ] List of requirements

## Budget (Optional)
- List of budgetary items that may need to be considered

/label ~rnd
/milestone %"R&D"
