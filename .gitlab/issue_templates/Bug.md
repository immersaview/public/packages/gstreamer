### Summary

(Summarize the bug encountered concisely)

### Steps to reproduce

(How one can reproduce the issue - this is very important)

### What is the current *bug* behavior?

(What actually happens)

### What is the expected *correct* behavior?

(What you should see instead)

### Reproducibility

Always | Once | Sometimes: (count reproduced out of attempts e.g. 1 in 5 attempts)

### Found In Versions

(Version issue was found including multiple versions if possible)

| Version | Issue Present |
| ------ | ------ |
| <app version/build> | Yes or No |

OS: (if applicable)

Hardware: (if applicable)

### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

Useful data:
- Log files
  - Upload whole file to GitLab issue.
  - Comment on excerpts from the log formatted with markdown code blocks (```)
- Screenshots
- Settings files

### Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug
