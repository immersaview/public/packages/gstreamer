## Description

As a [end-user for this story] I want [desired functionality] so that [end goal].

## Acceptance Criteria
- [ ] List of criteria

## Notes
- things that may be helpful to know/consider

### Definition of done
A copy of the definition of done can be found [here](http://wiki.immersaview.com/docs/handbook/definition-of-done/).

/label ~"user-story"
